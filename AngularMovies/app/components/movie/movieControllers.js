﻿'use strict';

(function () {
    var app = angular.module('movieModule');

    app.controller('MovieCtrl', ['movieService',
      function (movieService) {
          var viewModel = this;
          viewModel.orderBy = 'release_date';
          viewModel.title = 'Now playing';

          // Load now playing movies
          movieService.getNowPlaying().success(function (response) {
              viewModel.movieList = response.results;
          });
      }]);

    app.controller('FavoriteCtrl', ['movieService',
      function (movieService) {
          var viewModel = this;
          viewModel.orderBy = 'release_date';
          viewModel.title = 'Favorites';
          viewModel.movieList = [];

          // Get favorite movie ids
          var favorites = movieService.getFavorites();

          // Load now playing movies
          for (var i = 0; i < favorites.length; i++) {
              movieService.getMovie(favorites[i]).success(function (response) {
                  viewModel.movieList.push(response);
              });
          }
    }]);

    app.controller('MovieDetailCtrl', ['$routeParams', '$q', 'movieService',
      function ($routeParams, $q, movieService) {
          var viewModel = this;
          var movieId = $routeParams.movieId;

          viewModel.isLoading = true;

          // Gets the data for the current movie
          movieService.getMovie(movieId, true).success(function (response) {
              viewModel.data = response;

              // Gets the cast for the current movie
              if (response.credits && response.credits.cast.length > 0) {
                  viewModel.cast = response.credits.cast.slice(0, 8);
              }

              // Gets the trailer
              if (response.trailers && response.trailers.youtube.length > 0) {
                  viewModel.trailer = "https://www.youtube.com/embed/" + response.trailers.youtube[0].source;
              }

              // Gets the reviews
              if (response.reviews) {
                  viewModel.reviews = response.reviews.results;
              }

              // Gets the similar movies
              if (response.similar) {
                  viewModel.similar = response.similar.results.splice(0, 6);
              }

              viewModel.isLoading = false;
          });

          // Favorites a movie
          this.addFavorite = function () {
              var favorites = movieService.getFavorites();

              if (this.isFavorite()) {
                  var index = favorites.indexOf(movieId);
                  favorites.splice(index, 1);
              }
              else {
                favorites.push(movieId);
              }

              movieService.setFavorites(favorites);
          };

          // Gets a value indicating whether a movie is a favorite
          this.isFavorite = function() {
              var favorites = movieService.getFavorites();
              return favorites.indexOf(movieId) > -1;
          };
      }]);
}());
