﻿'use strict';

(function () {
    var searchService = function (api) {
        var base_url = 'search/movie';

        var findMovies = function (query, page) {
            return api.getResource(base_url, { query: query, sort_by: 'release_date.desc', page: page || 1 });
        }

        return {
            findMovies: findMovies
        };

    };

    // Register service
    var app = angular.module('searchModule');
    app.factory('searchService', ['apiService', searchService]);
}());
