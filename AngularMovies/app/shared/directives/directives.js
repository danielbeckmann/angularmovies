﻿'use strict';

(function () {
    var app = angular.module('shared');

    app.directive('loadingIndicator', function () {
        return {
            restrict: 'E',
            scope: {
                loading: '='
            },
            template: '<div id="veil" ng-show="loading"></div><div id="loading" ng-show="loading"><img src="/assets/img/loading.gif" /><br />Loading...</div>'
        };
    });
}());
