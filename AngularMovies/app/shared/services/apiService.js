﻿'use strict';

(function () {
    var apiService = function ($http) {
        var base_url = 'http://api.themoviedb.org/3/';
        var api_key = '2ea6fec8e68b11a77d52e5a0ff2dcb73';

        // Enable to use proxy - api_key can then be removed from this file
        // var base_url = 'http://localhost:65511?action=';

        var image_base_url = 'http://image.tmdb.org/t/p/';

        // Loads the api configuration
        var loadConfiguration = function () {
            var config = localStorage.getItem('movie-app-config');
            if (config) {
                // Use local stored data
                setConfigValues(JSON.parse(config));
            }
            else {
                // Get live data
                getResource('configuration').success(function (response) {
                    localStorage.setItem('movie-app-config', JSON.stringify(response));
                    setConfigValues(response);
                });
            }
        };

        // Performs a GET request on a resource on the api
        var getResource = function (action, params) {
            params = params || {};
            params.api_key = api_key;

            var url = base_url + action;
            return $http.get(url, { cache: true, params: params });
        }

        var getImageBaseUrl = function () {
            return image_base_url;
        };

        function setConfigValues(config) {
            image_base_url = config.images.base_url;
        }

        // Init
        loadConfiguration();

        return {
            getResource: getResource,
            getImageBaseUrl: getImageBaseUrl
        };
    };

    // Register service
    var app = angular.module('shared');
    app.factory('apiService', ['$http', apiService]);
}());
