﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

namespace MovieDatabaseProxy
{
    /// <summary>
    /// Proxy class for movie db requests to hide the api key from the user.
    /// </summary>
    public class Proxy : IHttpHandler
    {
        /// <summary>
        /// The url of the api.
        /// </summary>
        private const string BaseUrl = "http://api.themoviedb.org/3/";

        /// <summary>
        /// The api key.
        /// </summary>
        private const string ApiKey = "2ea6fec8e68b11a77d52e5a0ff2dcb73";

        /// <summary>
        /// Processes the request.
        /// </summary>
        /// <param name="context">The http context</param>
        public void ProcessRequest(HttpContext context)
        {
            var webClient = new WebClient();

            // Get the requested api action
            var action = context.Request.QueryString["action"];
            if (action != null)
            {
                // Build url with api key
                string request = GetParameterisedUri(string.Format("{0}{1}?api_key={2}", BaseUrl, action, ApiKey), context.Request.QueryString);

                try
                {
                    // Get response from api
                    webClient.Encoding = UTF8Encoding.UTF8;
                    string response = webClient.DownloadString(request);

                    // Set content type
                    context.Response.ContentType = "application/json";
                    context.Response.Charset = Encoding.UTF8.WebName;

                    // Enable CORS
                    context.Response.Headers.Add("Access-Control-Allow-Origin", "*");

                    // Write response back to user
                    context.Response.Write(response);
                }
                catch (WebException e)
                {
                    HttpWebResponse response = (HttpWebResponse)e.Response;
                    context.Response.StatusCode = (int)response.StatusCode;
                    context.Response.End();
                }
            }
            else
            {
                // Action not provided
                context.Response.StatusCode = 404;
                context.Response.End();
            }
        }

        /// <summary>
        /// Adds query params to an uri.
        /// </summary>
        /// <param name="uri">The base uri</param>
        /// <param name="queryParams">List of query parameters to add</param>
        /// <returns>Uri with added parameters</returns>
        private string GetParameterisedUri(string uri, NameValueCollection queryParams)
        {
            if (queryParams == null) return uri;

            var sb = new StringBuilder();

            var separatorChar = '?';
            if (uri.IndexOf('?') > -1)
            {
                separatorChar = '&';
            }

            foreach (string key in queryParams)
            {
                if (key == "action") continue;

                var value = queryParams[key];
                sb.Append(string.Format("{0}{1}={2}", separatorChar, key, WebUtility.UrlEncode(value)));

                // Toggle seperator after first parameter
                separatorChar = '&';
            }

            return uri + sb;
        }

        /// <summary>
        /// Gets a value indicating whether the http handler is reusable.
        /// </summary>
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}